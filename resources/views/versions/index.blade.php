@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">versions <a href="{{ route('versions.create', ['slug' => $plugin->slug])}}" style="float:right;"><input class="btn btn-primary pull-right" value="Create a new version"></a></div>

                <div class="card-body">
                <table class="table">
                <thead>
                    <tr>
                        <th>Version</th>
                        <th>requires</th>
                        <th>tested</th>
                        <th>change log</th>
                        <th class="text-right">Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($plugin->versions as $version)
                    <tr>    
                        <td>{{ $version->version }}</td>
                        <td>{{ $version->requires }}</td>
                        <td>{{ $version->tested }}</td>
                        <td>{{ $version->change_log }}</td>
                        <td class="text-right">
                            <a class='btn btn-secondary btn-xs' href="{!! route('versions.download', ['slug' => $plugin->slug, 'version' => $version->id]) !!}"><span class="glyphicon glyphicon-edit"></span> Download</a>
                            <a class='btn btn-info btn-xs' href="{!! route('versions.edit', ['slug' => $plugin->slug, 'version' => $version->id]) !!}"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                            {{ Form::open(['route' => ['versions.destroy', 'slug' => $plugin->slug, 'version' => $version->id], 'method' => 'post', 'style' => 'display:inline']) }}
                            {{ csrf_field() }}
                             {{ method_field('DELETE') }}
                             <input type="submit" class="btn btn-danger btn-xs" value="Del">

                            {{ Form::close() }}
                    </tr>
                @endforeach
                </tbody>
                 </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
