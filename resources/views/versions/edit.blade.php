@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Update {{ $plugin->plugin_name }}  -> {{ $version->version }}</div>

                <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error) 
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                {!! Form::model($version, ['route' => ['versions.update', 'slug' => $plugin->slug, 'version' => $version->id]]) !!}
                {{ method_field('PUT') }}
                @csrf
                    <div class="form-group">
                        <label>Version number</label>
                        {{ Form::text('version', $version->version, ['class' => 'form-control','required' => 'required']) }}
                        <small id="pluginHelp" class="form-text text-muted">Enter a version number for your release</small>
                    </div>
                    <div class="form-group">
                        <label>requires</label>
                        {{ Form::text('requires', $version->requires, ['class' => 'form-control']) }}
                        <small id="requiresHelp" class="form-text text-muted">for example 4.5</small>
                    </div>
                    <div class="form-group">
                        <label>tested</label>
                        {{ Form::text('tested', $version->tested, ['class' => 'form-control']) }}
                        <small id="requiresHelp" class="form-text text-muted">for example 4.5</small>
                    </div>
                    <div class="form-group">
                        <label>Change log</label>
                        {{ Form::textarea('change_log', $version->change_log, ['class' => 'form-control']) }}
                        <small id="descriptionHelp" class="form-text text-muted">The description will be visable for wordpress users.</small>
                    </div>

                    <button type="submit" class="btn btn-primary">Update Version</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
