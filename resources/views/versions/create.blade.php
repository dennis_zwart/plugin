@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create a new Version</div>

                <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error) 
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                {{ Form::open(['method' => 'post','route' => ['versions.store', 'slug' => $plugin->slug] , 'files'=>'true']) }}
                    @csrf
                    <div class="form-group">
                        <label>Version number</label>
                        <input type="text" class="form-control" name="version" aria-describedby="versionHelp" placeholder="Enter version (1.0.0)" value="{{ old('version') }}" required>
                        <small id="pluginHelp" class="form-text text-muted">Enter a version number for your release</small>
                    </div>
                    <div class="form-group">
                        <label>requires</label>
                        <input type="text" class="form-control" name="requires" aria-describedby="requiresHelp" placeholder="Enter requires(1.0)" value="{{ old('requires') }}">
                        <small id="requiresHelp" class="form-text text-muted">for example 4.5</small>
                    </div>
                    <div class="form-group">
                        <label>tested</label>
                        <input type="text" class="form-control" name="tested" aria-describedby="requiresHelp" placeholder="Enter tested(1.0)" value="{{ old('tested') }}">
                        <small id="requiresHelp" class="form-text text-muted">for example 4.5</small>
                    </div>
                    <div class="form-group">
                        <label>Change log</label>
                        <textarea class="form-control" name="change_log" aria-describedby="descriptionHelp" value="{{ old('description') }}"></textarea>
                        <small id="descriptionHelp" class="form-text text-muted">The description will be visable for wordpress users.</small>
                    </div>
                    <div class="form-group">
                        <label>Plugin file</label>
                        <input type="file" class="form-control-file" name="file" value="{{ old('plugin') }}" required>
                        <small accept=".zip" class="form-text text-muted">The plugin has to be packed into a zip file.</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Create Version</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
