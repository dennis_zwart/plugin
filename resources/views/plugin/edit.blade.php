@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Edit a plugin
                     <ul class="nav nav-tabs card-header-tabs pull-right"  id="myTab" role="tablist">
                        <li class="nav-item">
                        <a class="nav-link active" id="settings-tab" data-toggle="tab" href="#settings" role="tab" aria-controls="settings" aria-selected="true">Settings</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="banner-tab" data-toggle="tab" href="#banner" role="tab" aria-controls="banner" aria-selected="false">Banner</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="screenshots-tab" data-toggle="tab" href="#screenshots" role="tab" aria-controls="screenshots" aria-selected="false">Screenshots</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error) 
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="settings" role="tabpanel" aria-labelledby="settings-tab">
                        {!! Form::model($plugin, ['route' => ['plugins.update', $plugin->id]]) !!}
                        {{ method_field('PATCH') }}
                        @csrf
                        <div class="form-group">
                            <label>Plugin name</label>
                            {{ Form::text('plugin_name', $plugin->plugin_name, ['class' => 'form-control','required' => 'required']) }}
                            <small id="pluginHelp" class="form-text text-muted">Enter a name for your plugin (this will be visable on the wordpress dashboard)</small>
                        </div>
                        <div class="form-group">
                            <label>Slug name</label>
                            {{ Form::text('slug', $plugin->slug, ['class' => 'form-control', 'required' => 'required']) }}
                            <small id="slugHelp" class="form-text text-muted">Note: The slugname will be used inside the update link (http://example.com/$slugname/update).</small>
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            {{ Form::textarea('description', $plugin->description, ['class' => 'form-control', 'required' => 'required']) }}
                            <small id="descriptionHelp" class="form-text text-muted">The description will be visable for wordpress users.</small>
                        </div>
                        <button type="submit" class="btn btn-primary">update plugin</button>
                        {!! Form::close() !!}
                    </div>
                    <div class="tab-pane fade" id="banner" role="tabpanel" aria-labelledby="banner-tab">
                        @if(!empty($link))
                        <div class="container" style="height:500px;">
                            <img src="{{ $link }}" style='max-width: 100%; max-height: 100%;'>
                        </div>
                        @endif
                        <file-upload :plugin="{{ $plugin->id }}"></file-upload>
                    </div>
                    <div class="tab-pane fade" id="screenshots" role="tabpanel" aria-labelledby="screenshots-tab">
                        <screenshot-manager plugin="{{ $plugin->slug }}"></screenshot-manager>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
