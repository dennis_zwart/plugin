@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create a new plugin</div>

                <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error) 
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="post" action="{{ route('plugins.store') }}">
                    @csrf
                    <div class="form-group">
                        <label>Plugin name</label>
                        <input type="text" class="form-control" name="plugin_name" aria-describedby="pluginHelp" placeholder="Enter plugin" required>
                        <small id="pluginHelp" class="form-text text-muted">Enter a name for your plugin (this will be visable on the wordpress dashboard)</small>
                    </div>
                    <div class="form-group">
                        <label>Slug name</label>
                        <input type="text" class="form-control" name="slug" aria-describedby="slugHelp" placeholder="Enter slug" required>
                        <small id="slugHelp" class="form-text text-muted">Note: The slugname will be used inside the update link (http://example.com/$slugname/update).</small>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="description" aria-describedby="descriptionHelp" required></textarea>
                        <small id="descriptionHelp" class="form-text text-muted">The description will be visable for wordpress users.</small>
                    </div>
                    <button type="submit" class="btn btn-primary">Create plugin</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
