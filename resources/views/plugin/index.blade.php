@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Plugins <a href="{{ route('plugins.create')}}"> <input class="btn btn-primary" style="float:right;" value="Create a new plugin"></a></div>

                <div class="card-body">
                <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>name</th>
                        <th>slug</th>
                        <th class="text-right">Action</th>
                    </tr>
                </thead>
                @foreach($plugins as $plugin)
                    <tr>
                        <td>{{ $plugin->id }}</td>
                        <td>{{ $plugin->plugin_name }}</td>
                        <td>{{ $plugin->slug }}</td>
                        <td class="text-right">
                            <a class='btn btn-secondary btn-xs' href="{!! route('versions.index', ['slug' => $plugin->slug]) !!}"><span class="glyphicon glyphicon-edit"></span> versions</a> 
                            <a class='btn btn-info btn-xs' href="{!! route('plugins.edit', [$plugin->id]) !!}"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                            {{ Form::open(['route' => ['plugins.destroy', $plugin->id], 'method' => 'post', 'style' => 'display:inline']) }}
                            {{ csrf_field() }}
                             {{ method_field('DELETE') }}
                             <input type="submit" class="btn btn-danger btn-xs" value="Del">
                            {{ Form::close() }}
                    </tr>
                @endforeach
                 </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
