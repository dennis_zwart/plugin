<?php

namespace App\Http\Controllers;

use App\Models\Plugin;
use App\Models\Version;
use Illuminate\Http\Request;

class VersionController extends Controller
{
    //
    /**
     * Show the version table for a specific plugin.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {

        $plugin = Plugin::where('slug', $slug)->with('versions')->first();
        return view('versions.index', compact('plugin'));
    }

    /**
     * Show the version table for a specific plugin.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function create($slug)
    {
        \Debugbar::info($slug);
        $plugin = Plugin::where('slug', $slug)->with('versions')->first();
        return view('versions.create', compact('plugin'));
    }

    /**
     * Show the version table for a specific plugin.
     *
     * @param Request $request
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $slug)
    {

        $request->validate([
            'version' => 'required|unique:versions',
            'requires' => 'required',
            'tested' => '',
            'change_log' => '',
            'file' => 'required|file|mimes:zip',
        ]);
        $plugin = Plugin::where('slug', $slug)->with('versions')->first();
        $request->file->storeAs($slug . '/' . $request->version, $request->file->getClientOriginalName());
        $version = new Version;
        $version->version = $request->version;
        $version->downloaded = 0;
        $version->plugin_id = $plugin->id;
        if (!empty($request->change_log)) {
            $version->change_log = $request->change_log;
        }
        if (!empty($request->requires)) {
            $version->requires = $request->requires;
        }
        if (!empty($request->tested)) {
            $version->tested = $request->tested;
        }
        $version->filename = $request->file->getClientOriginalName();
        $version->save();
        return redirect()->route('versions.index', ['slug' => $slug]);
    }

    /**
     * Show the version edit form.
     *
     * @param  Request $request
     * @param  string $slug
     * @param integer $version
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $slug, $version)
    {
        $plugin = Plugin::where('slug', $slug)->first();
        $version = Version::find($version);
        return view('versions.edit', compact('plugin', 'version'));
    }

    /**
     * Update the version inside the database.
     *
     * @param  Request $request
     * @param  string $slug
     * @param integer $version
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug, $version)
    {
        $version = Version::find($version);
        $request->validate(
            [
                'version' => 'required|unique:versions,version,' . $version->version . ',version',
                'requires' => 'required',
                'tested' => '',
                'change_log' => '',
            ]
        );
        $plugin = Plugin::where('slug', $slug)->first();

        $version->version = $request->version;
        if (!empty($request->change_log)) {
            $version->change_log = $request->change_log;
        }
        if (!empty($request->requires)) {
            $version->requires = $request->requires;
        }
        if (!empty($request->tested)) {
            $version->tested = $request->tested;
        }
        $version->save();
        return redirect()->route('versions.index', ['slug' => $slug]);
    }

    /**
     * destroy the version inside the database.
     *
     * @param  Request $request
     * @param  string $slug
     * @param integer $version
     * @return \Illuminate\Http\Response
     */

    public function destroy(Request $request, $slug, $version)
    {
        $version = Version::find($version);

        if (!empty($version)) {
            $path = storage_path('app/public/' . $slug . '/' . $version->version);
            if (file_exists($path)) {
                \File::deleteDirectory($path);
            }
            $version->delete();
        }
        return redirect()->route('versions.index', ['slug' => $slug]);
    }

    /**
     * Download any given version.
     *
     * @param string $slug
     * @param integer $version_id
     * @return \Illuminate\Http\Response
     */

    public function downloadVersion($slug, $version_id)
    {
        $version = Version::find($version_id);
        if (!empty($version_id)) {
            $path = storage_path('app/public/' . $slug . '/' . $version->version . '/' . $version->filename);
            if (file_exists($path)) {
                return response()->download($path);
            } else {
                return response('File not found', 200);
            }
        }
    }
}
