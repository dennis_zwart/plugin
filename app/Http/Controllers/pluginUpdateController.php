<?php

namespace App\Http\Controllers;

use App\Models\Plugin;
use App\Models\Version;
use App\Models\Images;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PluginUpdateController extends Controller
{
    //
    /**
     * Show the specified photo comment.
     *
     * @param  string  $slug
     * @return Response
     */
    public function pluginDetails(Request $request, $slug)
    {
        $plugin = Plugin::with('latestVersion', 'banner')->where('slug', $slug)->first();
        if (!empty($plugin)) {
            $obj = new \stdClass();
            $obj->name = $plugin->plugin_name;
            $obj->slug = $plugin->slug;
            $obj->download_url = "https://plugins.voltinternet.nl/" . $slug . "/download";
            $obj->version = $plugin->latestVersion->version;
            $obj->requires = $plugin->latestVersion->requires;
            $obj->tested = $plugin->latestVersion->tested;
            if(!empty($plugin->banner)) {
                $imgSlug = $plugin->banner->slug;
                $obj->banner = url("/image/$imgSlug");
            }
            $obj->last_updated = Carbon::parse($plugin->latestVersion->updated_at)->toDateTimeString();
            $obj->upgrade_notice = "Plugin update is available.";
            $obj->author = "Volt Internet";
            $obj->author_homepage = "https://voltinternet.nl";
            $obj->sections = array(
                'description' => $plugin->description,
                'installation' => "Upload the plugin to your blog, Activate it, that's it!",
                'changelog' => $plugin->latestVersion->change_log,
            );
            $json = json_encode($obj, JSON_UNESCAPED_SLASHES);
            return response($json);
        } else {
            echo 'Plugin not found';
        }
    }
    /**
     * Show the specified photo comment.
     *
     * @param  string  $slug
     * @return Response
     */
    public function downloadPlugin($slug)
    {

        $plugin = Plugin::with('latestVersion')->where('slug', $slug)->first();
        if (!empty($plugin)) {
            if (!empty($plugin->latestVersion)) {
                $filename = $plugin->latestVersion->filename;
                return response()->download(storage_path('app/public/' . $slug . '/' . $plugin->latestVersion->version . '/' . $filename));
            } else {
                echo 'No versions found for this plugin ';
            }
        } else {
            echo 'Plugin not found';
        }
    }

    /**
     * Show the specified photo comment.
     *
     * @param  string  $slug
     * @return Response
     */
    public function returnImage(String $slug)
    {
        $image = Images::where('slug', '=', $slug)->with('plugin')->first();
        if (!empty($image)) {
            return \Image::make(storage_path("app/public/{$image->plugin->slug}/images/{$image->filename}"))->response();
        }
        return 'not found';
    }
}
