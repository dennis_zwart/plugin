<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Image;
use App\Models\Images;
use App\Models\Plugin;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;

class ApiController extends Controller
{
    //
    public function uploadBanner(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        } else {
            $plugin = Plugin::find($request->get('plugin'));
            if (!empty($plugin)) {
                $imageData = $request->get('image');
                $fileExtention =  explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
                $path = "{$plugin->slug}/images/";
                Storage::makeDirectory($path);
                $filename = str_random(8);
                $image = Images::where('plugin_id', '=', $plugin->id)->where('type', '=', 'Banner')->first();
                if (!empty($image)) {
                    unlink(storage_path("app/public/{$plugin->slug}/images/{$image->filename}"));
                    $image->delete();
                }
                try {
                    $image = new Images;
                    $image->plugin_id = $plugin->id;
                    $image->type = "Banner";
                    $image->filename = $filename . '.' . $fileExtention;
                    $image->slug = $filename;
                    $image->save();
                    Image::make($imageData)->save(storage_path("app/public/{$plugin->slug}/images/") . $filename . '.' . $fileExtention);
                } catch (\Exception $ex) {
                    return response()->json(['errors'=>$ex]);
                }
            }
            return response()->json(['errors'=>'none']);
        }
    }

    public function returnScreenshotLinks($slug)
    {
        $plugin = Plugin::where('slug', $slug)->with('screenshots')->first();
        if(!empty($plugin)) {
            if(!empty($plugin->screenshots)) {
                $links = [];
                foreach($plugin->screenshots as $screenshot) {
                    $linkBuilder = ['id' => $screenshot->id, 'url' => url("/image/$screenshot->slug")];
                    array_push($links, $linkBuilder);
                }
                $json = json_encode($links, JSON_UNESCAPED_SLASHES);
                return response($json, 200);
            } else {
                return response()->json([['errors' => 'No screenshots found']]);
            }
        }
    }
}
