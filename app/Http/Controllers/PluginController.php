<?php

namespace App\Http\Controllers;

use App\Models\Plugin;
use Illuminate\Http\Request;

class PluginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $plugins = Plugin::all();
        return view('plugin.index', compact('plugins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('plugin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'plugin_name' => 'required|unique:plugins|max:255',
            'slug' => 'required|unique:plugins',
            'description' => 'required',
        ]);
        $plugin = new Plugin;
        $plugin->slug = $request->slug;
        $plugin->plugin_name = $request->plugin_name;
        $plugin->downloaded = 0;
        $plugin->description = $request->description;
        $plugin->save();
        return redirect()->route('plugins.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $plugin = Plugin::find($id)->with('banner')->first();
        if (!empty($plugin->banner)) {
            $slug = $plugin->banner->slug;
            $link = url("/image/$slug");
        }
        return view('plugin.edit', compact('plugin', 'link'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $plugin = Plugin::find($id);
        $request->validate([
            'plugin_name' => 'required|max:255',
            'slug' => 'required|unique:plugins,slug,' . $plugin->slug . ',slug',
            'description' => 'required',
        ]);

        $path = storage_path('app/public/' . $plugin->slug);
        $newPath = storage_path('app/public/' . $request->slug);
        $success = \File::moveDirectory($path, $newPath);

        $plugin->slug = $request->slug;
        $plugin->plugin_name = $request->plugin_name;
        $plugin->description = $request->description;
        $plugin->save();

        return redirect()->route('plugins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $plugin = Plugin::find($id);
        $path = storage_path('app/public/' . $plugin->slug);
        \File::deleteDirectory($path);
        $plugin->delete();
        return redirect()->back();
    }
}
