<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plugin extends Model
{
    //
    protected $table = 'plugins';

    protected $fillable = ['slug', 'plugin_name', 'downloaded', 'description'];


    public function latestVersion()
    {
        return $this->hasOne('App\Models\Version', 'plugin_id', 'id')->latest();
    }
    public function versions()
    {
        return $this->hasMany('App\Models\Version', 'plugin_id', 'id')->latest();
    }
    public function banner() 
    {
        return $this->hasOne('App\Models\Images', 'plugin_id', 'id')->where('type', '=', 'banner');
    }

    public function screenshots() 
    {
        return $this->hasMany('App\Models\Images', 'plugin_id', 'id')->where('type', '=', 'Screenshot');
    }
}
