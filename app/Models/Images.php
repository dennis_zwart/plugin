<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    //
    protected $table = 'images';

    protected $fillable = ['plugin_id', 'type', 'filename', 'slug'];

    public function plugin() 
    {
        return $this->belongsTo('App\Models\Plugin', 'plugin_id', 'id');
    }
}
