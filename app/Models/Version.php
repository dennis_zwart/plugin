<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Version extends Model
{
    //

    protected $table = 'versions';

    protected $fillable = ['plugin_id', 'version', 'change_log', 'requires', 'tested', 'file_name'];

    public function plugin()
    {
        return $this->belongsTo('App\Models\Plugin', 'id', 'plugin_id');
    }
}
