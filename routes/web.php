<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/{slug}/download', 'PluginUpdateController@downloadPlugin');
Route::get('/image/{slug}', 'PluginUpdateController@returnImage');
Route::get('/{slug}/update', 'PluginUpdateController@pluginDetails');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', 'PluginController@index')->name('home');
    Route::get('/{slug}/versions', 'VersionController@index')->name('versions.index');
    Route::get('/{slug}/versions/create', 'VersionController@create')->name('versions.create');
    Route::post('/{slug}/versions', 'VersionController@store')->name('versions.store');
    Route::get('/{slug}/versions/{version}/edit', 'VersionController@edit')->name('versions.edit');
    Route::put('/{slug}/versions/{version}/store', 'VersionController@update')->name('versions.update');
    Route::delete('/{slug}/versions/{version}/delete', 'VersionController@destroy')->name('versions.destroy');
    Route::get('/{slug}/versions/{version}/download', 'VersionController@downloadVersion')->name('versions.download');
    Route::resource('/plugins', 'PluginController');
});
