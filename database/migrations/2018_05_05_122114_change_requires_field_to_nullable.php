<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRequiresFieldToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('versions', function(Blueprint $table) {
            $table->string('requires')->nullable()->change();
            $table->string('tested')->nullable()->change();
            $table->string('change_log')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('versions', function(Blueprint $table) {
            $table->string('requires')->nullable(false)->change();
            $table->string('tested')->nullable(false)->change();
            $table->string('change_log')->nullable(false)->change();
        });
    }
}
