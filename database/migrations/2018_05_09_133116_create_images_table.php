<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->integer('plugin_id')->unsigned()->nullable();
            $table->string('type');
            $table->string('filename');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::table('images', function (Blueprint $table) {
            $table->foreign('plugin_id')
                ->references('id')
                ->on('plugins')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
