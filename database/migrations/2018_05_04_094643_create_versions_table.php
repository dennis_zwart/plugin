<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('versions', function (Blueprint $table) {
            //
            $table->increments('id');
            $table->integer('plugin_id')->unsigned()->nullable();
            $table->integer('version');
            $table->text('change_log');
            $table->integer('requires');
            $table->integer('tested');
            $table->string('filename');
            $table->timestamps();
        });

        Schema::table('versions', function ($table) {
            $table->foreign('plugin_id')->references('id')->on('plugins')->onDelete('cascade');;
        }); 
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
            Schema::dropIfExists('versions');
    }
}
